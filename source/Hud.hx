package;

import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import openfl.net.SharedObject;

using flixel.util.FlxSpriteUtil;

class Hud extends FlxTypedGroup<FlxSprite>
{
    private var _sprBack:FlxSprite;
    private var _txtMoney:FlxText;
    private var score:Int = 0;
    private var so:SharedObject;

    static inline var HEIGHT=50;

    public function new()
    {
        super();

	so = SharedObject.getLocal('caveData');
	if(so.data.bestScore == null) so.data.bestScore = 0;
        // _sprBack = new FlxSprite().makeGraphic(FlxG.width, HEIGHT+1, FlxColor.BLACK);
        // _sprBack.drawRect(0, HEIGHT, FlxG.width, 0, FlxColor.WHITE);
	// add(_sprBack);

        //_txtMoney = new FlxText(0, 2, FlxG.width, "", 48);
        _txtMoney = new FlxText(0, 2, 0, "", 48);
        _txtMoney.setBorderStyle(SHADOW, FlxColor.GRAY, 1, 1);
        _txtMoney.alignment = LEFT;
        _txtMoney.x = FlxG.width - 200;
        add(_txtMoney);
        forEach(function(spr:FlxSprite)
        {
            spr.scrollFactor.set(0, 0);
        });
    }

    override public function update(elapsed:Float):Void 
    {
	if(so.data.bestScore < score) {
            so.data.bestScore = score;
            so.flush();
	}
        _txtMoney.text = so.data.bestScore + "\n" + (++score);
    }
}

