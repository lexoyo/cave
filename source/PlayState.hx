package;

import flash.Lib;
import flash.display.BlendMode;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.nape.FlxNapeSpace;
import flixel.math.FlxMath;
import flixel.math.FlxRect;
import flixel.util.FlxColor;
import nape.geom.Vec2;

using flixel.util.FlxSpriteUtil;

/**
 * @author TiagoLr ( ~~~ProG4mr~~~ )
 */
class PlayState extends FlxState
{
	private var orb:Orb;
	private var hud:Hud;
	private var terrain:Terrain;
	override public function create():Void 
	{
		FlxNapeSpace.init();
		super.create();
		
    #if !FLX_NO_MOUSE
		FlxG.mouse.visible = false;
    #end
		FlxG.autoPause = false;
		FlxNapeSpace.space.gravity = new Vec2(0, 600);
	    FlxNapeSpace.space.worldLinearDrag = .25;
	    FlxNapeSpace.space.worldAngularDrag = .25;
		FlxNapeSpace.createWalls(0, 0, FlxG.stage.stageWidth, FlxG.stage.stageHeight);

		orb = new Orb(Math.round(FlxG.width * .25), Math.round(FlxG.height * .5), "assets/Orb.png");
		add(orb);


		terrain = new Terrain();
		add(terrain);
		
		hud = new Hud();
		add(hud);
		
		// FlxG.camera.follow(orb, LOCKON, 1);
	}

	override public function update(elapsed:Float):Void 
	{	
		super.update(elapsed);

		if(!orb.alive) {
			FlxG.camera.shake();
			haxe.Timer.delay(function() FlxG.switchState(new PlayState()), 1000);
		}
		var speed = 20;
    #if !FLX_NO_MOUSE
      		var pressed:Bool = FlxG.mouse.pressed || FlxG.keys.anyPressed([W, UP, SPACE, ENTER]);
		if (FlxG.keys.anyPressed([LEFT]))
			orb.body.applyImpulse(new Vec2(-speed, 0));
		if (FlxG.keys.anyPressed([RIGHT]))
			orb.body.applyImpulse(new Vec2(speed, 0));
    #else
      var pressed:Bool = FlxG.touches.getFirst() != null;
    #end
		if (pressed)
			orb.body.applyImpulse(new Vec2(0, -speed));
		
	}
}
