# Cave

A game with a ball and a cave. Touch or click or space to go up.

Made with Haxe, haxeflixel, nape. Build with
```
$ lime build html5
```

![Screenshot](./screenshot.png)

Work in progress, still to do
* deployment to gitlab pages
* build native app
* optim: use nape filters to avoid collision check between tiles?
* UI: at least a score

Notes:

* test on android with `ANDROID_SDK_ROOT=/opt/android-sdk/ ANDROID_NDK_ROOT=/opt/android-ndk-r14b ANDROID_SDK=/opt/android-sdk lime test android -simulator -debug` (I had to mkdir /opt/android-sdk/tools && cp -r ~/Downloads/ant/ /opt/android-sdk/tools)
* gameplay ideas
  * 2 walkers (2 caves)
  * 2 players
  * sounds
  * breakable tiles (hit => allowPhysics)
