package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.addons.nape.FlxNapeSprite;
import openfl.Assets;

import flixel.system.FlxAssets;
import nape.dynamics.*;

class Tile extends FlxNapeSprite
{
	public var shadow:FlxSprite;
	
	public function new (X:Int, Y:Int, ?SimpleGraphic:FlxGraphicAsset)
	{
		super(X, Y, SimpleGraphic);
		createCircularBody(graphic.height / 2);
		body.allowMovement = false;
		body.allowRotation = false;
		//body.setShapeFilters(new InteractionFilter(1, 2));
	}
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
	}
}
