package;

import flixel.util.*;
import flixel.*;
import flixel.group.*;

import openfl.Assets;
import flixel.system.FlxAssets;
import openfl.display.BitmapData;
import flixel.group.FlxSpriteGroup;
import flixel.math.*;

class Terrain extends FlxGroup
{
  var imgWidth:Float;
  var imgHeight:Float;
  var nTilesX:Int;
  var nTilesY:Int;
  var walkerY:Float; // walk up and down to draw the cave
  var walkerYSpeed:Float;
  var walkerMaxHeight:Int;
  public function new(maxSize:Int = 0)
  {
    super(maxSize);
	var	floorImg = Assets.getBitmapData("assets/hexagon.png");
	imgWidth = 2 + Math.ceil(.75 * floorImg.width);
	imgHeight = 1 + floorImg.height;
	// det the required number of tiles
	nTilesX = 1 + Math.ceil(FlxG.stage.stageWidth / imgWidth);
	// the cols are alternate (1 up, 1 down) so always even num of tiles in X
	if(nTilesX%2 == 1) nTilesX++;
	nTilesY = 1 + Math.ceil(FlxG.stage.stageHeight / imgHeight);
	// walker params
	walkerMaxHeight = FlxG.stage.stageHeight;
	walkerY = rand(walkerMaxHeight/2, [-walkerMaxHeight/3, walkerMaxHeight/3]);
	walkerYSpeed = 0;
	// start: create all the tiles
	createFloorTiles(floorImg, FlxG.stage.stageWidth + 100);
  }

	var tiles:Array<Array<Tile>>;
	private function createFloorTiles(floorImg:BitmapData, offsetX) 
	{
		tiles = [];
		for(i in 0...nTilesX)  
		{
			tiles[i] = [];
			var tileX:Int = Math.round(i * imgWidth);
			for(j in 0...nTilesY)  
			{
				var tileY:Int = Math.round(j * imgHeight - (i%2 == 0 ? imgHeight / 2 : 0));
				var tile = new Tile(tileX + offsetX, tileY, floorImg);
				add(tile);
				tiles[i][j] = tile;
			}
		}
	}
	var firstRecycleOccured = false;
	private function updateFloorTiles(offsetX:Float) 
	{
		var edgeTiles = [];
		var recycledCol = false;
		for(i in 0...nTilesX)  
		{
			for(j in 0...nTilesY)  
			{
				var justRecycledTile = false;
				var tile = tiles[i][j];
				tile.body.position.x += Math.round(offsetX);
				//tile.body.velocity.x = offsetX;
				// recycle the tile
				if(tile.body.position.x < -imgWidth) {
					//tile.body.position.x = Math.ceil((nTilesX - 1) * imgWidth) ;
					// at the right of the previous tile
					var prevIdx = i == 0 ? tiles.length-1 : i-1;
					tile.body.position.x = Math.round(tiles[prevIdx][j].body.position.x + imgWidth + (i == 0 ? offsetX : 0));
					recycledCol = true;
					firstRecycleOccured = true;
					justRecycledTile = true;
				}
				// hide or show depending on the walker
				// for tiles not yet visible
				if(
					// case before tiles reach the left corner
					(firstRecycleOccured == false &&
					tile.body.position.x >= FlxG.stage.stageWidth + imgWidth &&
					tile.body.position.x < FlxG.stage.stageWidth + 2 * imgWidth) ||
					// case we just recycled the current tile
					justRecycledTile
				) {
					// no physics by default
					tile.physicsEnabled = false;
					tile.alpha = .7;
					// show or hide depending on the distance to the walker
					if(Math.abs(tile.body.position.y - walkerY) < caveHeight) {
						// hide this tile
						tile.exists = false;
						// store as an edge tile if previous tile was visible
						if(j-1 > 0 && tiles[i][j-1].exists) {
							edgeTiles.push({
								i: i, j: j,
								tile: tiles[i][j-1],
							});
						}
						randomReplace(tile);
					}
					else {
						// show this tile
						tile.exists = true;
						// store as an edge tile if previous tile was hidden
						if(j-1 > 0 && !tiles[i][j-1].exists) {
							edgeTiles.push({
								i: i, j: j,
								tile: tile,
							});
						}
					}
				}
			}
		}
		// handle physics at the edges
		for(idx in 0...edgeTiles.length)  
		{
			// find nighbors of each edge tile
			var obj = edgeTiles[idx];
			var neighbours= [];
			var distX = 1;
			var distY = 1;
			for(i in obj.i-distX...obj.i+distX) {
				for(j in obj.j-distY...obj.j+distY) {
					if(i>=0 && j>=0 && i<nTilesX && j<nTilesY && (i!=obj.i || j!=obj.j)) {
						neighbours.push(tiles[i][j]);
					}
				}
			}
			// acivate physics for all the neighbors
			neighbours.map(function(tile) {
				// FIXME: why this is needed? if removed there are wholes in the edge tiles
				tile.exists = true;
				if(tile.exists) {
					tile.physicsEnabled = true;
					tile.alpha = 1;
				}
				return true;
			});
			// FIXME: why this is needed? if removed there are wholes in the edge tiles
			obj.tile.physicsEnabled = true;
			// highlight the edge tiles for debug
			obj.tile.exists = true;
			obj.tile.alpha = .5;
		}
		// the left col has been moved to the right
		// update the walker
		if(recycledCol) {
			// do nothing?!
			// walkerYSpeed = rand(walkerYSpeed, [-10, 10], [-100, 100]);
			// walkerY += 40 * walkerYSpeed;
			// walkerY = rand(walkerY, null, [0, walkerMaxHeight]);
			// caveHeight = 40 * rand(caveHeight, [-100, 100], [200, 450]);
		}
	}
	function randomReplace(tile:Tile) {
		if(random.bool(1)) {
			// var clone = cast(tile.clone());
			// clone.body.allowMovement = true;
			// clone.physicsEnabled = true;
			// clone.exists = true;
			// clone.alpha = .2;
		}
	}
	var scrollSpeed:Float = -5;
	var walkerCrazyness:Float = 2;
	var caveHeight:Float = 300;
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		//scrollSpeed = rand(scrollSpeed, [-.1, .1], [-25, -5]);
		scrollSpeed -= .001;

		walkerCrazyness += .01;
		//walkerCrazyness = Math.min(Math.max(walkerCrazyness, 2), 6);
		
		walkerYSpeed = rand(walkerYSpeed, [-walkerCrazyness, walkerCrazyness], [-10, 10]);
		walkerY += walkerYSpeed;
		walkerY = rand(walkerY, null, [0, walkerMaxHeight]);
		
		// caveHeight = rand(caveHeight, [-10, 10], [150, 350]);
		caveHeight -= .01;
		//caveHeight = Math.min(Math.max(caveHeight, FlxG.stage.stageHeight * .1), FlxG.stage.stageHeight * .4);

		updateFloorTiles(scrollSpeed);
	}
	var random = new FlxRandom();
  function rand(value:Float, rndRange:Array<Float> = null, range:Array<Float> = null):Float {
    var res:Float = value;
    if(rndRange != null) {
      res += random.float(rndRange[0], rndRange[1]);
    }
    if(range != null) {
      res = Math.min(res, range[1]);
      res = Math.max(res, range[0]);
    }
    return res;
  }

}
