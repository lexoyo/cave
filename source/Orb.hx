package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.addons.nape.FlxNapeSprite;
import openfl.Assets;

import flixel.system.FlxAssets;
import nape.dynamics.*;

class Orb extends FlxNapeSprite
{
	public var shadow:FlxSprite;
	
	public function new (X:Int, Y:Int, ?SimpleGraphic:FlxGraphicAsset, CreateRectangularBody:Bool = true, EnablePhysics:Bool = true)
	{
		super(X, Y, SimpleGraphic, CreateRectangularBody, EnablePhysics);
		createCircularBody(18);
		body.allowRotation = false;
		//body.setShapeFilters(new InteractionFilter(2, 2));
	}
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		if(body.position.x < (width / 3)) {
			// trace("position out", width, body.position.x);
			kill();
			return;
		}
		if(
		   //body.position.x < 100 
		   //&& 
		   body.crushFactor() > 2000
		 ) {
			//trace("crush", body.crushFactor(), body.position.x);
			//kill();
			//return;
		}
		if(body.tangentImpulse().length>0) {
			//trace("impulse", body.tangentImpulse().x, body.position.x);
			//kill();
			//return;
		}
	}
}
